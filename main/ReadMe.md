'A1Q2.py' takes a .csv file as input from the user and computes
Mean, Median, Mode, Standard Deviation and Variance of each numeric
column in the CSV file. The Results are stored in 
'output_<INPUT_FILE_NAME>.csv' in the data folder.